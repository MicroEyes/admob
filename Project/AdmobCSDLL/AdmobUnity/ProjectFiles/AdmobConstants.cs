﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.microeyes.admob
{
    public class AdmobConstants
    {
        private AdmobConstants(){}

        public const string SettingsLocation = "Assets/Resources/AdmobSettings.asset";
        public const string TAG = "Admob";

        public const string GameObjectName = "__Admob";
    }
}
