﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.microeyes.admob
{
    internal class CS_Java_Bridge
    {
        static CS_Java_Bridge s_instance = null;
        static CS_Java_Bridge Instance
        {
            get 
            {
                if (s_instance == null)
                {
                    s_instance = new CS_Java_Bridge();
                }
                return s_instance; 
            }
        }

        static AndroidJavaClass admobPluginClass;
        static AndroidJavaClass unityPlayer;
        static AndroidJavaObject currActivity;

        //Preventing new from Outside Class
        private CS_Java_Bridge()
        {

        }

        internal static void Initialize()
        {
            //Debug.Log(AdmobConstants.TAG + ": Instantiate Called");
            admobPluginClass = new AndroidJavaClass("com.microeyes.admob.AdmobActivity");
            unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            currActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            admobPluginClass.CallStatic
                (
                    "Initialize",
                    currActivity,
                    AdmobManager.Settings.AdUnitID,
                    //AdmobManager.Settings.AdUnit_Interstitial,
                    AdmobManager.Settings.IsTesting,
                    AdmobManager.Settings.TestDeviceId,
                    AdmobConstants.GameObjectName,
                    (int)AdmobManager.Settings.LevelOfDebug
                );

            //Debug.Log(AdmobConstants.TAG + "Instantiate FINISHED");
        }


        #region Banner Requests

        internal static void Banner_Create(BannerAd a_bannerAd)
        {
            admobPluginClass.CallStatic
                (
                    "Banner_Create", 
                    a_bannerAd.ID,
                    a_bannerAd.AdUnitId,
                    (int)a_bannerAd.BannerType, 
                    (int)a_bannerAd.Orientation, 
                    (int)a_bannerAd.PositionOne, 
                    (int)a_bannerAd.PositionTwo, 
                    (int)a_bannerAd.AnimInType,
                    (int)a_bannerAd.AnimOutType
                );
        }

        internal static void Banner_Load(BannerAd a_bannerAd)
        {
            admobPluginClass.CallStatic
                (
                    "Banner_Load",
                    a_bannerAd.ID
                );
        }

        internal static void Banner_Hide(BannerAd a_bannerAd)
        {
            admobPluginClass.CallStatic
                (
                    "Banner_Hide",
                    a_bannerAd.ID
                );
        }

        internal static void Banner_Show(BannerAd a_bannerAd)
        {
            admobPluginClass.CallStatic
                (
                    "Banner_Show",
                    a_bannerAd.ID
                );
        }

        internal static void Banner_Reposition(BannerAd a_bannerAd)
        {
            admobPluginClass.CallStatic
                (
                    "Banner_Reposition",
                    a_bannerAd.ID,
                    (int) a_bannerAd.PositionOne,
                    (int) a_bannerAd.PositionTwo
                );
        }

        #endregion


        #region Interstitial

        internal static void Interstitial_Create(InterstitialAd a_interAdv)
        {
            admobPluginClass.CallStatic
                (
                    "Interstitial_Create",
                    a_interAdv.ID,
                    a_interAdv.AdUnitId
                );
        }

        internal static void Interstitial_Load(InterstitialAd a_interAdv)
        {
            admobPluginClass.CallStatic
                (
                    "Interstitial_Load",
                    a_interAdv.ID
                );
        }

        internal static void Interstitial_Show(InterstitialAd a_interAdv)
        {
            admobPluginClass.CallStatic
                (
                    "Interstitial_Show",
                    a_interAdv.ID
                );
        }

        internal static void Interstitial_IsLoaded(InterstitialAd a_interAdv)
        {
            admobPluginClass.CallStatic
                (
                    "Interstitial_IsLoaded",
                    a_interAdv.ID
                );
        }

        #endregion

        internal static void RemoveAd(string a_id)
        {
            admobPluginClass.CallStatic
                (
                    "RemoveAd",
                    a_id
                );
        }

        internal static void RemoveAdImmediately(string a_id)
        {
            admobPluginClass.CallStatic
                (
                    "RemoveAdImmediately",
                    a_id
                );
        }

    }
}
