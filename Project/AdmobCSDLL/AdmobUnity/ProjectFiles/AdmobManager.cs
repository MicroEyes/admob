﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace com.microeyes.admob
{
    #region ENUMS

    public enum EAdType
    {
        Banner,
        Interstitial
    }

    public enum EAnimationInType
    {
        SLIDE_IN_LEFT,
        FADE_IN,
        NO_ANIMATION
    };

    public enum EAnimationOutType
    {
        SLIDE_OUT_RIGHT,
        FADE_OUT,
        NO_ANIMATION
    };

    public enum ELevelOfDebug
    {
        NONE,
        LOW,
        HIGH,
        FLOOD
    }

    public enum EAdErrorCode
    {
        ERROR_CODE_INTERNAL_ERROR = 0,
        ERROR_CODE_INVALID_REQUEST = 1,
        ERROR_CODE_NETWORK_ERROR = 2,
        ERROR_CODE_NO_FILL = 3,
    }

    internal enum EAdDisplayEvent
    {
        OnAdOpened,
        OnAdLeftApplication,
        OnAdClosed,
        OnAdLoaded,
        OnAdFailedToLoad
    }

    #endregion

    #region Callback Delegates

    public delegate void Argument_Zero();
    public delegate void Argument_One_ID_ONLY(string a_id);
    public delegate void Argument_ErrorCode(string a_id, EAdErrorCode a_errorCode);

    #endregion

    public class AdmobManager
    {
        #region Initialization
        private static AdmobManager s_instance = null;
        private static AdmobManager Instance
        {
            get 
            {
                if (s_instance == null)
                {
                    Debug.Log("Assigning AdmobManager Singleton");
                    s_instance = new AdmobManager();
                    s_instance.InitializeGameObject();
                    s_instance.m_lstAdvType.Clear();
                    s_instance.LoadSettings();

                    CS_Java_Bridge.Initialize();

                    //s_instance.s_isInitialized = true;
                }
                return s_instance; 
            }
        }

        void LoadSettings()
        {
            UnityEngine.Object l_obj = Resources.Load("AdmobSettings");
            if (l_obj != null)
            {
                m_settings = (AdmobSettings)l_obj;
                Debug.Log(m_settings.ToString());
            }
            else
            {
                Debug.LogError("AdmobManager::LoadSettings:: Settings file not found");
            }
        }

        void InitializeGameObject()
        {
            GameObject l_gameObject = new GameObject(AdmobConstants.GameObjectName);
            l_gameObject.AddComponent<Java_CS_Bridge>();
        }
#endregion

        #region Variables

        //private bool s_isInitialized = false;
        //internal static bool IsInitialized
        //{
        //    get 
        //    { 
        //        return Instance.s_isInitialized; 
        //    }
        //}

        private Dictionary<string, AdmobAdvBase> m_lstAdvType = new Dictionary<string, AdmobAdvBase>();
        internal static Dictionary<string, AdmobAdvBase> LstAdvType
        {
            get { return Instance.m_lstAdvType; }            
        }

        private AdmobSettings m_settings;
        internal static AdmobSettings Settings
        {
            get { return Instance.m_settings; }
        }
#endregion

        public static T Prepare<T>(string a_id) where T : AdmobAdvBase
        {
            if (!Utilities.IsRunningAndroid)
                return null;

            if (IsAdPresent(a_id))
            {
                Debug.LogWarning("Ad ID: '" + a_id + "' already created");
                return null;
            }

            AdmobAdvBase l_adRef = null;

            if (typeof(T).Equals(typeof(BannerAd)))
            {
                l_adRef = new BannerAd(a_id);
                Instance.m_lstAdvType.Add(a_id, l_adRef);
            }
            else if (typeof(T).Equals(typeof(InterstitialAd)))
            {
                l_adRef = new InterstitialAd(a_id);
                Instance.m_lstAdvType.Add(a_id, l_adRef);
            }
            else
            {
                Debug.LogWarning("AdmobManager::Create:: Not Handled AdType: " + typeof(T));
            }

            return (T)l_adRef;
        }

        public static BannerAd PrepareBannerAd(string a_id)
        {
            if (!Utilities.IsRunningAndroid)
                return null;

            return Prepare<BannerAd>(a_id);
        }

        public static InterstitialAd PrepareInterstitialAd(string a_id)
        {
            if (!Utilities.IsRunningAndroid)
                return null;

            return Prepare<InterstitialAd>(a_id);
        }

        public static void Load(string a_id)
        {
            if (!Utilities.IsRunningAndroid)
                return;

            AdmobAdvBase l_base = Get(a_id);
            if (l_base != null)
            {
                if (l_base.AdType == EAdType.Banner)
                {
                    BannerAd l_bannerAd = (BannerAd)l_base;
                    l_bannerAd.Load();
                }
                else if (l_base.AdType == EAdType.Interstitial)
                {
                    InterstitialAd l_interAd = (InterstitialAd)l_base;
                    l_interAd.Load();
                }
                else
                {
                    Debug.LogWarning("AdmobManager::Load:: AdType not handled '" + l_base.AdType + "'");
                }
            }
            else
            {
                Debug.LogWarning("AdmobManager::Load:: Can't find ID '" +  a_id + "'");
            }

        }

        private static bool RemoveAd(string a_id)
        {
            if (!Utilities.IsRunningAndroid)
                return false;

            AdmobAdvBase l_adRef = Get<AdmobAdvBase>(a_id);
            if (l_adRef != null)
            {
                Instance.m_lstAdvType.Remove(a_id);
                CS_Java_Bridge.RemoveAd(a_id);
                return true;
            }
            else
            {
                Debug.LogWarning("AdmobManager::Remove:: Can't find ID: '" + a_id + "' to remove");
            }

            return false;
        }

        public static bool RemoveAdImmediately(string a_id)
        {
            if (!Utilities.IsRunningAndroid)
                return false;

            AdmobAdvBase l_adRef = Get<AdmobAdvBase>(a_id);
            if (l_adRef != null)
            {
                Instance.m_lstAdvType.Remove(a_id);
                CS_Java_Bridge.RemoveAdImmediately(a_id);
                return true;
            }
            else
            {
                Debug.LogWarning("AdmobManager::Remove:: Can't find ID: '" + a_id + "' to remove");
            }

            return false;
        }

        public static T Get<T>(string a_id) where T : AdmobAdvBase
        {
            if (!Utilities.IsRunningAndroid)
                return null;

            AdmobAdvBase l_adRef = null;

            if (Instance.m_lstAdvType.TryGetValue(a_id, out l_adRef))
            {
                //Debug.Log("Ad ID '" + a_id + "' Found");
                //Do nothing
            }

            return (T)l_adRef;
        }

        private static AdmobAdvBase Get(string a_id)
        {
            if (!Utilities.IsRunningAndroid)
                return null;

            AdmobAdvBase l_adRef = null;

            if (Instance.m_lstAdvType.TryGetValue(a_id, out l_adRef))
            {
                //Debug.Log("Ad ID '" + a_id + "' Found");
                //Do nothing
            }

            return l_adRef;
        }

        private static bool IsAdPresent(string a_id)
        {
            return Instance.m_lstAdvType.ContainsKey(a_id);
        }

        internal static void TriggerEvent(string a_id, EAdDisplayEvent a_event, object a_extraData = null)
        {
            if (!Utilities.IsRunningAndroid)
                return;

            Debug.Log("AdmobManager::TriggerEvent:: ID: " + a_id + ", Event: " + a_event);

            AdmobAdvBase l_baseAdRef = Get<AdmobAdvBase>(a_id);
            if (l_baseAdRef != null)
            {
                switch (a_event)
                {
                    case EAdDisplayEvent.OnAdOpened:
                        if (l_baseAdRef.OnAdOpened != null)
                        {
                            l_baseAdRef.OnAdOpened.Invoke(a_id);
                        }
                        break;
                    case EAdDisplayEvent.OnAdLeftApplication:
                        if (l_baseAdRef.OnAdLeftApplication != null)
                        {
                            l_baseAdRef.OnAdLeftApplication.Invoke(a_id);
                        }
                        break;
                    case EAdDisplayEvent.OnAdClosed:
                        if (l_baseAdRef.OnAdClosed != null)
                        {
                            l_baseAdRef.OnAdClosed.Invoke(a_id);
                        }
                        break;
                    case EAdDisplayEvent.OnAdLoaded:
                        if (l_baseAdRef.OnAdLoaded != null)
                        {
                            l_baseAdRef.OnAdLoaded.Invoke(a_id);
                        }
                        break;
                    case EAdDisplayEvent.OnAdFailedToLoad:
                        if (l_baseAdRef.OnAdFailedToLoad != null)
                        {
                            EAdErrorCode l_errorCode = (EAdErrorCode)a_extraData;
                            l_baseAdRef.OnAdFailedToLoad.Invoke(a_id, l_errorCode);
                        }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Debug.LogWarning("AdmobManager::TriggerEvent:: Ad is null for ID '" + a_id + "', Event: " + a_event);
            }
        }

        internal static void OnRequestCompleted(string a_data)
        {
            string[] l_arrData = a_data.Split('^');
            string l_advId = l_arrData[0];

            AdmobAdvBase l_base = Get(l_advId);
            if (l_base != null)
            {
                if (l_base.AdType == EAdType.Interstitial)
                {
                    InterstitialAd l_intertitialAd = (InterstitialAd)l_base;
                    l_intertitialAd.OnRequestCompleted(a_data);
                }
                else
                {
                    Debug.LogWarning("AdmobManager::OnRequestCompleted:: AdType not handled:: " + l_base.AdType);
                }
            }

        }

        public static void HideBanner(string a_id)
        {
            if (!Utilities.IsRunningAndroid)
                return;

            AdmobAdvBase l_base = Get(a_id);
            if (l_base != null)
            {
                if (l_base.AdType == EAdType.Banner)
                {
                    BannerAd l_bannerAd = (BannerAd)l_base;
                    l_bannerAd.Hide();
                }
                else
                {
                    Debug.LogWarning("Admob::HideBanner:: Ad ID: '" + a_id + "' is not BannerAd.");
                }
            }
        }

        public static void ShowBanner(string a_id)
        {
            if (!Utilities.IsRunningAndroid)
                return;

            AdmobAdvBase l_base = Get(a_id);
            if (l_base != null)
            {
                if (l_base.AdType == EAdType.Banner)
                {
                    BannerAd l_bannerAd = (BannerAd)l_base;
                    l_bannerAd.Show();
                }
                else
                {
                    Debug.LogWarning("Admob::HideBanner:: Ad ID: '" + a_id + "' is not BannerAd.");
                }
            }
        }

        public static void ShowInterstitial(string a_id)
        {
            if (!Utilities.IsRunningAndroid)
                return;

            AdmobAdvBase l_base = Get(a_id);
            if (l_base != null)
            {
                if (l_base.AdType == EAdType.Interstitial)
                {
                    InterstitialAd l_interAd = (InterstitialAd)l_base;
                    l_interAd.Show();
                }
                else
                {
                    Debug.LogWarning("Admob::HideBanner:: Ad ID: '" + a_id + "' is not InterstitialAd.");
                }
            }
        }
    }
}