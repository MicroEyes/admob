﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.microeyes.admob
{
    public class Java_CS_Bridge : MonoBehaviour
    {
        [SerializeField]
        private int m_iAdCount = 0;

        void Awake()
        {
            DontDestroyOnLoad(this);
        }

        void Start()
        {
            m_iAdCount = 0;
        }

        void FixedUpdate()
        {
            m_iAdCount = AdmobManager.LstAdvType.Count;
        }

        #region Callback from Admob Plugin

        public void onAdLoaded(string a_advId)
        {
            Debug.Log(AdmobConstants.TAG + "Admob Callback - OnReceiveAd: " + a_advId);
            AdmobManager.TriggerEvent(a_advId, EAdDisplayEvent.OnAdLoaded);
        }

        /// <summary>
        /// Called when an ad was not received
        /// </summary>
        /// <param name="a_data">Contains Error message</param>
        public void onAdFailedToLoad(string a_data)
        {
            Debug.Log(AdmobConstants.TAG + "Admob Callback - OnFailedToReceiveAd: " + a_data);
            
            string[] l_arrData = a_data.Split('^');
            string l_advId = l_arrData[0];
            EAdErrorCode l_errorCode = (EAdErrorCode)(Convert.ToInt32(l_arrData[1]));

            AdmobManager.TriggerEvent(l_advId, EAdDisplayEvent.OnAdFailedToLoad, l_errorCode);
        }

        /// <summary>
        /// Called when an Activity is created in front of the app
        /// </summary>
        /// <param name="a_advId">Contains empty message</param>
        public void onAdClosed(string a_advId)
        {
            Debug.Log(AdmobConstants.TAG + "Admob Callback - OnPresentScreen: " + a_advId);
            AdmobManager.TriggerEvent(a_advId, EAdDisplayEvent.OnAdClosed);
        }

        /// <summary>
        /// Called when an ad is clicked and about to return to the application
        /// </summary>
        /// <param name="a_advId">Contains empty message</param>
        public void onAdOpened(string a_advId)
        {
            Debug.Log(AdmobConstants.TAG + "Admob Callback - OnDismissScreen: " + a_advId);
            AdmobManager.TriggerEvent(a_advId, EAdDisplayEvent.OnAdOpened);
        }

        /// <summary>
        /// Called when an ad is clicked and going to start a new Activity that will leave the application
        /// </summary>
        /// <param name="a_advId">Contains empty message</param>
        public void onAdLeftApplication(string a_advId)
        {
            Debug.Log(AdmobConstants.TAG + "Admob Callback - OnLeaveApplication: " + a_advId);
            AdmobManager.TriggerEvent(a_advId, EAdDisplayEvent.OnAdLeftApplication);
        }

        public void OnRequestCompleted(string a_data)
        {
            Debug.Log(AdmobConstants.TAG + "Admob Callback - OnRequestCompleted: " + a_data);
            AdmobManager.OnRequestCompleted(a_data);
        }

        #endregion
    }    
    
}
