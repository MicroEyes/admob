﻿using UnityEngine;
using System.Collections;

namespace com.microeyes.admob
{
    #region ENUMS

    public enum EBannerAdType
    {
        BANNER = 0,
        IAB_BANNER = 1,
        IAB_MRECT = 2,
        IAB_LEADERBOARD = 3,
        SMART_BANNER = 4,
        WIDE_SKYSCRAPER = 5,
    }

    public enum EOrientation
    {
        VERTICAL,
        HORIZONTAL
    };

    public enum EPosition
    {
        NO_GRAVITY = 0,
        CENTER_HORIZONTAL = 1,
        LEFT = 3,
        RIGHT = 5,
        FILL_HORIZONTAL = 7,
        CENTER_VERTICAL = 16,
        CENTER = 17,
        TOP = 48,
        BOTTOM = 80,
        FILL_VERTICAL = 112
    };
#endregion

    public class BannerAd : AdmobAdvBase
    {
        #region Variables

        private EBannerAdType m_bannerType = EBannerAdType.SMART_BANNER;

        public EBannerAdType BannerType
        {
            get { return m_bannerType; }
            set { m_bannerType = value; }
        }

        private EOrientation m_orientation = EOrientation.HORIZONTAL;

        public EOrientation Orientation
        {
            get { return m_orientation; }
            set { m_orientation = value; }
        }

        private EPosition m_positionOne = EPosition.TOP;

        public EPosition PositionOne
        {
            get { return m_positionOne; }
        }

        private EPosition m_positionTwo = EPosition.CENTER_HORIZONTAL;

        public EPosition PositionTwo
        {
            get { return m_positionTwo; }
        }


        private EAnimationInType m_animInType = EAnimationInType.NO_ANIMATION;

        public EAnimationInType AnimInType
        {
            get { return m_animInType; }
            set { m_animInType = value; }
        }

        private EAnimationOutType m_animOutType = EAnimationOutType.NO_ANIMATION;

        public EAnimationOutType AnimOutType
        {
            get { return m_animOutType; }
            set { m_animOutType = value; }
        }
        
#endregion

        public BannerAd(string a_id)
        {
            ID = a_id;
            AdType = EAdType.Banner;
        }

        public void SetPosition(EPosition a_position_1)
        {
            m_positionOne = a_position_1;
        }

        public void SetPosition(EPosition a_position_1, EPosition a_position_2)
        {
            m_positionOne = a_position_1;
            m_positionTwo = a_position_2;
        }

        public void Create()
        {
            //Debug.Log("BannerAd::ID-" + ID + "::Create::" + AdmobManager.IsInitialized);
            CS_Java_Bridge.Banner_Create(this);
        }
         
        public void Load()
        {
            CS_Java_Bridge.Banner_Load(this);
        }

        public void Hide()
        {
            CS_Java_Bridge.Banner_Hide(this);
        }

        public void Show()
        {
            CS_Java_Bridge.Banner_Show(this);
        }

        public void Reposition(EPosition a_position_1, EPosition a_position_2)
        {
            SetPosition(a_position_1, a_position_2);
            CS_Java_Bridge.Banner_Reposition(this);
        }
    }
}
