﻿using UnityEngine;
using System.Collections;

namespace com.microeyes.admob
{
    public class AdmobAdvBase
    {
        [SerializeField]
        private string m_id = string.Empty;
        public string ID
        {
            get { return m_id; }
            protected set { m_id = value; }
        }

        [SerializeField]
        private EAdType m_adType = EAdType.Banner;
        public EAdType AdType
        {
            get { return m_adType; }
            protected set { m_adType = value; }
        }

        [SerializeField]
        private string m_AdUnitId = string.Empty;
        public string AdUnitId
        {
            get { return m_AdUnitId; }
            set { m_AdUnitId = value; }
        }

        private Argument_One_ID_ONLY m_onAdOpened = null;
        public Argument_One_ID_ONLY OnAdOpened
        {
            get { return m_onAdOpened; }
            set { m_onAdOpened = value; }
        }

        private Argument_One_ID_ONLY m_onAdLeftApplication = null;
        public Argument_One_ID_ONLY OnAdLeftApplication
        {
            get { return m_onAdLeftApplication; }
            set { m_onAdLeftApplication = value; }
        }

        private Argument_One_ID_ONLY m_onAdClosed = null;
        public Argument_One_ID_ONLY OnAdClosed
        {
            get { return m_onAdClosed; }
            set { m_onAdClosed = value; }
        }

        private Argument_One_ID_ONLY m_onAdLoaded = null;
        public Argument_One_ID_ONLY OnAdLoaded
        {
            get { return m_onAdLoaded; }
            set { m_onAdLoaded = value; }
        }

        private Argument_ErrorCode m_onAdFailedToLoad = null;
        public Argument_ErrorCode OnAdFailedToLoad
        {
            get { return m_onAdFailedToLoad; }
            set { m_onAdFailedToLoad = value; }
        }

        /// <summary>
        /// Index of function in Data String
        /// </summary>
        public const int INDEX_FUNCTION_NAME = 1;

        protected AdmobAdvBase()
        {

        }

        internal virtual void OnRequestCompleted(string a_data)
        {

        }

    }

}