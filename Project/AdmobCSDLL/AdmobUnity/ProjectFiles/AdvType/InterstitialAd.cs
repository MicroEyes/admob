﻿using UnityEngine;
using System;
using System.Collections;

namespace com.microeyes.admob
{
    public class InterstitialAd : AdmobAdvBase
    {
        /// <summary>
        /// Local function index keeper
        /// </summary>
        public const int Func_IsLoaded = 0;

        /// <summary>
        /// Value of boolean isLoaded in 'Data' String
        /// </summary>
        public const int VALUE_ISLOADED = 2;

        Action m_ActionIsAdLoaded = null;
        Action m_ActionIsAdNotLoaded = null;

        public InterstitialAd(string a_id)
        {
            ID = a_id;
            AdType = EAdType.Interstitial;
        }

        public void Create()
        {
            //Debug.Log("BannerAd::ID-" + ID + "::Create::" + AdmobManager.IsInitialized);
            CS_Java_Bridge.Interstitial_Create(this);
        }

        public void Load()
        {
            CS_Java_Bridge.Interstitial_Load(this);
        }

        public void Show()
        {
            CS_Java_Bridge.Interstitial_Show(this);
        }

        public void IsLoaded(Action a_isAdLoaded, Action a_isAdNotLoaded)
        {
            m_ActionIsAdLoaded = a_isAdLoaded;
            m_ActionIsAdNotLoaded = a_isAdNotLoaded;
            CS_Java_Bridge.Interstitial_IsLoaded(this);
        }

        internal override void OnRequestCompleted(string a_data)
        {
            string[] l_arrData = a_data.Split('^');
            int l_functionToCall = Convert.ToInt32(l_arrData[AdmobAdvBase.INDEX_FUNCTION_NAME]);
            switch (l_functionToCall)
            {
                case InterstitialAd.Func_IsLoaded:
                    bool l_isLoaded = (Convert.ToInt32(l_arrData[VALUE_ISLOADED]) == 0) ? false : true;
                    if (l_isLoaded)
                    {
                        if (m_ActionIsAdLoaded != null)
                        {
                            m_ActionIsAdLoaded.Invoke();
                        }
                        else
                        {
                            Debug.Log("InterstitialAd::OnRequestCompleted::IsLoaded:: ID: " + ID + ". No OnAdLoaded Action attached");
                        }
                    }
                    else
                    {
                        if (m_ActionIsAdNotLoaded != null)
                        {
                            m_ActionIsAdNotLoaded.Invoke();
                        }
                        else
                        {
                            Debug.Log("InterstitialAd::OnRequestCompleted::IsLoaded:: ID: " + ID + ". No OnAdNotLoaded Action attached");
                        }
                    }
                    break;

                default:
                    Debug.LogWarning("InterstitialAd::OnRequestCompleted:: AdId: " + ID + ":: Invalid function call index:" + l_functionToCall);
                    break;
            }
        }

    }
}
