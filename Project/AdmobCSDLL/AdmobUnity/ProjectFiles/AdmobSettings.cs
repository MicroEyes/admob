﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace com.microeyes.admob
{
    public class AdmobSettings : UnityEngine.ScriptableObject
    {
        [SerializeField]
        private string m_strAdUnitId = string.Empty;
        public string AdUnitID
        {
            get { return m_strAdUnitId; }
            set { m_strAdUnitId = value; }
        }

        //[SerializeField]
        //private string m_strAdUnitId_Interstitial = string.Empty;
        //public string AdUnit_Interstitial
        //{
        //    get { return m_strAdUnitId_Interstitial; }
        //    set { m_strAdUnitId_Interstitial = value; }
        //}

        [SerializeField]
        private bool m_isTesting = false;
        /// <summary>
        /// Set True, if testing game/app
        /// </summary>        
        public bool IsTesting
        {
            get { return m_isTesting; }
            set { m_isTesting = value; }
        }

        [SerializeField]
        private string m_testDeviceId = "6F1CC2C5371933EEDDACB0BF7F42917F";
        /// <summary>
        /// Test Device Id, if you know
        /// </summary>
        public string TestDeviceId
        {
            get { return m_testDeviceId; }
            set { m_testDeviceId = value; }
        }

        [SerializeField]
        private ELevelOfDebug m_levelOfDebug = ELevelOfDebug.LOW;

        public ELevelOfDebug LevelOfDebug
        {
            get { return m_levelOfDebug; }
            set { m_levelOfDebug = value; }
        }

        public AdmobSettings()
        {
            m_strAdUnitId = string.Empty;
            //m_strAdUnitId_Interstitial = string.Empty;
            m_isTesting = false;
            m_testDeviceId = string.Empty;
        }

        public override string ToString()
        {
            return "AdmobSettings:: AdUnitID_Banner: " + AdUnitID + ", IsTesting: " + IsTesting + ", TestDeviceID: " + TestDeviceId + ", LevelOfDebug: " + LevelOfDebug;
        }
    }
}
