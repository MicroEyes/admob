﻿using System;
using UnityEngine;

namespace com.microeyes.admob
{
    internal class Utilities
    {
        internal static bool IsRunningAndroid
        {
            get
            {
                if (!Application.isEditor && Application.isPlaying && Application.platform == RuntimePlatform.Android)
                {
                    return true;
                }
                else
                {
                    Debug.LogWarning("AdmobManager works only on Android Device");
                    return false;
                }
            }
        }
    }

}
