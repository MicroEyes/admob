package com.microeyes.utils;

import java.util.Hashtable;

import com.google.android.gms.ads.AdSize;
import com.microeyes.admob.catalog.EBannerAdType;

public class Constants {
	public static String APP_TAG = "Admob";
	public static final Hashtable<Integer, EBannerAdType> AdvSizeMap;
	static
	{
		AdvSizeMap = new Hashtable<Integer, EBannerAdType>();
		AdvSizeMap.put(0, EBannerAdType.BANNER);
		AdvSizeMap.put(1, EBannerAdType.FULL_BANNER);
		AdvSizeMap.put(2, EBannerAdType.LEADERBOARD);
		AdvSizeMap.put(3, EBannerAdType.MEDIUM_RECTANGLE);
		AdvSizeMap.put(4, EBannerAdType.SMART_BANNER);
		AdvSizeMap.put(5, EBannerAdType.WIDE_SKYSCRAPER);
	}
	
	public static final Hashtable<EBannerAdType, AdSize> AdvSizeInternal_Map_2_GoogleSize;
	static
	{
		AdvSizeInternal_Map_2_GoogleSize = new Hashtable<EBannerAdType, AdSize>();
		AdvSizeInternal_Map_2_GoogleSize.put(EBannerAdType.BANNER, AdSize.BANNER );
		AdvSizeInternal_Map_2_GoogleSize.put(EBannerAdType.FULL_BANNER, AdSize.FULL_BANNER);
		AdvSizeInternal_Map_2_GoogleSize.put(EBannerAdType.LEADERBOARD, AdSize.LEADERBOARD);
		AdvSizeInternal_Map_2_GoogleSize.put(EBannerAdType.MEDIUM_RECTANGLE, AdSize.MEDIUM_RECTANGLE);
		AdvSizeInternal_Map_2_GoogleSize.put(EBannerAdType.SMART_BANNER, AdSize.SMART_BANNER);
		AdvSizeInternal_Map_2_GoogleSize.put(EBannerAdType.WIDE_SKYSCRAPER, AdSize.WIDE_SKYSCRAPER);
	}
	
}
