package com.microeyes.utils;

import android.util.Log;

public class Debug {
	 
	  public enum DebugLevel
	  {
		  NONE(0),
	      LOW(1),
	      HIGH(2),
	      FLOOD(3);
		  
		  public final int m_value;
		  DebugLevel(int a_value)  {
			  this.m_value = a_value;
		  }
		  
		  public int getValue() {
			  return this.m_value;
		  }		  
	  }
	  
	  private static DebugLevel m_debugLevel = DebugLevel.NONE;
	  
	  /**
	   * Set Level of Debug
	   * @param a_debugLevel
	   */
	  public static void SetDebugLevel(int a_debugLevel) {
		  m_debugLevel = DebugLevel.values()[a_debugLevel];
	  }	  
	  
	  /**
	   * Display Log from this functino
	   * @param a_caller	Unique Identifier	
	   * @param a_level		Level of Debug to show this Log
	   * @param a_log		Log String
	   */
	  public static void Log(String a_caller, DebugLevel a_level, String a_log)
	  {
		  if(m_debugLevel.getValue() >= a_level.getValue())
		  {
			  Log.i(Constants.APP_TAG, a_caller + "________ " + a_log);
		  }
	  }
	  
	  public static void LogWarning(String a_caller, String a_log) {
		Log.w(Constants.APP_TAG, a_caller + "__________" + a_log);		  
	  }
	
}
