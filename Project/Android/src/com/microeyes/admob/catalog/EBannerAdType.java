package com.microeyes.admob.catalog;

public enum EBannerAdType {

	BANNER(0),
	FULL_BANNER(1),
	LEADERBOARD(2),
	MEDIUM_RECTANGLE(3),
	SMART_BANNER(4),
	WIDE_SKYSCRAPER(5);

	private final int value;
	private EBannerAdType(int a_value)
	{
		value = a_value;
	}
	
	public int getValue(){
		return value;
	}
	
}
