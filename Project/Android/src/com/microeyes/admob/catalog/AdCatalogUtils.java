package com.microeyes.admob.catalog;

import com.google.android.gms.ads.*;
import com.microeyes.admob.AdmobActivity;

import android.content.Context;
import android.content.res.Configuration;

public class AdCatalogUtils {
	/**
	   * Prevent instantiation.
	   */
	  private AdCatalogUtils() {
	    // Empty.
	  }

	  /**
	   * Determines whether or not the device has an extra large screen.
	   *
	   * @param context The Android context.
	   * @return boolean value indicating if the screen size is extra large.
	   */
	  public static boolean isExtraLargeScreen(Context context) {
	    int screenSizeMask = context.getResources().getConfiguration().screenLayout
	        & Configuration.SCREENLAYOUT_SIZE_MASK;
	    if (screenSizeMask == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
	      return true;
	    } else {
	      return false;
	    }
	  }

	  /**
	   * Creates an ad request.  It will be a test request if test mode is enabled.
	   *
	   * @return An AdRequest to use when loading an ad.
	   */
	  public static AdRequest createAdRequest() {		
	    AdRequest adRequest = null;
	    if (AdmobActivity.isTestMode) {
	      // This call will add the emulator as a test device.  To add a physical
	      // device for testing, pass in your hashed device ID, which can be found
	      // in the LogCat output when loading an ad on your device.
	    	adRequest = new AdRequest.Builder()
	    					.addTestDevice(AdmobActivity.testDeviceId)
	    					.build();
	    }
	    else {
	    	adRequest = new AdRequest.Builder().build();
	    }
	    return adRequest;
	  }
}
