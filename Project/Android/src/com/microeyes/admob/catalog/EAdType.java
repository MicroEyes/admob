package com.microeyes.admob.catalog;

public enum EAdType {

	BANNER(0),
	INTERSTITIAL(1);

	private final int value;
	private EAdType(int a_value) {
		value = a_value;
	}
	
	public int getValue(){
		return value;
	}
}
