package com.microeyes.admob;

import com.google.android.gms.ads.*;
import com.microeyes.admob.catalog.AdCatalogUtils;
import com.microeyes.utils.Debug;
import com.microeyes.utils.Debug.DebugLevel;

public class InterstitialAdv extends BaseAdvertisement {

	public static int Func_IsLoaded = 0;
	
	int position_1 = -1;
	int position_2 = -1;

	private InterstitialAd m_interstitial;

	public InterstitialAdv(String identifier) {
		super(identifier);
		
		Debug.Log("InterstitialAdv::InterstitialAdv", DebugLevel.HIGH,
				"Creating InterstitialAdv object");	
				

		Debug.Log("InterstitialAdv::InterstitialAdv", DebugLevel.HIGH,
				"Created InterstitialAdv object");
	}

	public void Load() {
		
		m_interstitial = new InterstitialAd(AdmobActivity.MainActivity);
		
		Debug.Log("InterstitialAd::Load", DebugLevel.FLOOD, "Using AdUnitId: " + getAdUnitId());
	    m_interstitial.setAdUnitId(getAdUnitId());
	    m_interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				onAdLoaded_Internal();
			}

			public void onAdFailedToLoad(int errorCode) {
				onAdFailedToLoad_Internal(errorCode);
			}

			public void onAdOpened() {
				onAdOpened_Internal();
			}

			public void onAdClosed() {
				onAdClosed_Internal();
			}

			public void onAdLeftApplication() {
				onAdLeftApplication_Internal();
			}
		});

	    // Create ad request.
	    adRequest = AdCatalogUtils.createAdRequest();

	    // Begin loading your interstitial.
	    m_interstitial.loadAd(adRequest);		
	}
		
	@Override
	public void onAdLoaded_Internal() {
		// TODO Auto-generated method stub
		super.onAdLoaded_Internal();
		SendToUnity_onAdLoaded();
	}

	@Override
	public void onAdFailedToLoad_Internal(int errorCode) {
		// TODO Auto-generated method stub
		super.onAdFailedToLoad_Internal(errorCode);
		SendToUnity_onAdFailedToLoad(errorCode);
	}

	@Override
	public void onAdOpened_Internal() {
		// TODO Auto-generated method stub
		super.onAdOpened_Internal();
		SendToUnity_onAdOpened();
	}

	@Override
	public void onAdClosed_Internal() {
		// TODO Auto-generated method stub
		super.onAdClosed_Internal();
		SendToUnity_onAdClosed();
	}

	@Override
	public void onAdLeftApplication_Internal() {
		// TODO Auto-generated method stub
		super.onAdLeftApplication_Internal();
		SendToUnity_onAdLeftApplication();
	}

	public void Show() {
		
		if(m_interstitial.isLoaded()) {
			m_interstitial.show();
		}
		else {
			Debug.Log("InterstitialAdv::Show", DebugLevel.HIGH,
					"Interstitial Adv not yet loaded");
		}
		
	}
	
	public boolean IsLoaded() {
		return m_interstitial.isLoaded();
	}
	

}
