package com.microeyes.admob;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.google.android.gms.ads.*;
import com.microeyes.admob.catalog.AdCatalogUtils;
import com.microeyes.admob.catalog.EAdType;
import com.microeyes.utils.Constants;
import com.microeyes.utils.Debug;
import com.microeyes.utils.Debug.DebugLevel;
import com.microeyes.admob.catalog.EBannerAdType;

public class BannerAdv extends BaseAdvertisement {

	protected Animation currAnimation_StartAd;
	protected Animation currAnimation_EndAd;
	protected LinearLayout layout = null;
	protected AdView adView = null;
	private boolean m_isFirstAdVisible = false;
	
	int m_position_1 = -1;
	int m_position_2 = -1;

	AdSize m_bannerSize;

	public BannerAdv(String identifier, final EBannerAdType a_bannerType, 
			final int Orientation, final int animationInType,
			final int animationOutType) {
		super(identifier);
		
		Debug.Log("BannerAdv::BannerAdv", DebugLevel.HIGH, "Creating bannerAdv object");
		
		m_bannerSize = Constants.AdvSizeInternal_Map_2_GoogleSize.get(a_bannerType);

		advOrientation = Orientation;
		SetAnimationInType(animationInType);
		SetAnimationOutType(animationOutType);
		setAdvType(EAdType.BANNER);
		
		Debug.Log("BannerAdv::BannerAdv", DebugLevel.HIGH, "Created bannerAdv object");
	}

	public void SetPositions(final int pos_1, final int pos_2) {
		m_position_1 = pos_1;
		m_position_2 = pos_2;
	}	
	
	public void Load() {
		layout = new LinearLayout(AdmobActivity.MainActivity);
		layout.setOrientation(advOrientation);
		layout.setGravity(m_position_1 | m_position_2);

		Debug.Log("BannerAdv::Load::ID=" + getIdentifier(),
				DebugLevel.HIGH, "Setup Layout complete");

		AdmobActivity.MainActivity.addContentView(layout, new LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT));

		// layout.setBackgroundColor(Color.RED);

		Debug.Log("BannerAdv::Load::ID=" + getIdentifier(),
				DebugLevel.HIGH, "Added new layout");

		adView = new AdView(AdmobActivity.MainActivity);
		
		Debug.Log("BannerAd::Load", DebugLevel.FLOOD, "Using AdUnitId: " + getAdUnitId());
		adView.setAdUnitId(getAdUnitId());
		adView.setAdSize(m_bannerSize);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
				onAdLoaded_Internal();
			}

			public void onAdFailedToLoad(int errorCode) {
				onAdFailedToLoad_Internal(errorCode);
			}

			public void onAdOpened() {
				onAdOpened_Internal();
			}

			public void onAdClosed() {
				onAdClosed_Internal();
			}

			public void onAdLeftApplication() {
				onAdLeftApplication_Internal();
			}
		});
		
		adView.setBackgroundColor(Color.TRANSPARENT);

		Debug.Log("BannerAdv::Load::ID=" + getIdentifier(),
				DebugLevel.HIGH, "Created new AdView");

		layout.addView(adView, new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));

		Debug.Log("BannerAdv::Load::ID=" + getIdentifier(),
				DebugLevel.HIGH, "Added new AdView in Layout");

		adRequest = AdCatalogUtils.createAdRequest();

		adView.loadAd(adRequest);
	}
	
	protected void SetAnimationInType(int animationInType) {
		Debug.Log("BannerAdv::SetAnimationInType", DebugLevel.HIGH,
				"Setting Animation IN Type: " + animationInType);
		try {
			switch (animationInType) {
			case 0:
				currAnimation_StartAd = AnimationUtils.loadAnimation(
						AdmobActivity.MainActivity.getApplicationContext(),
						android.R.anim.slide_in_left);
				break;

			case 1:
				currAnimation_StartAd = AnimationUtils.loadAnimation(
						AdmobActivity.MainActivity.getApplicationContext(),
						android.R.anim.fade_in);
				break;

			case 2:
				currAnimation_StartAd = null;
				break;
			}
		} catch (Exception e) {
			Log.e(Constants.APP_TAG, e.getMessage()
					+ ". Using default animation");
			currAnimation_StartAd = AnimationUtils.loadAnimation(AdmobActivity.MainActivity,
					android.R.anim.slide_in_left);
		}
	}

	protected void SetAnimationOutType(int animationOutType) {
		Debug.Log("BannerAdv::SetAnimationOutType", DebugLevel.HIGH,
				"Setting Animation OUT Type: " + animationOutType);

		try {
			switch (animationOutType) {

			case 0:
				currAnimation_EndAd = AnimationUtils.loadAnimation(
						AdmobActivity.MainActivity.getApplicationContext(),
						android.R.anim.slide_out_right);
				break;

			case 1:
				currAnimation_EndAd = AnimationUtils.loadAnimation(
						AdmobActivity.MainActivity.getApplicationContext(),
						android.R.anim.fade_out);
				break;

			case 2:
				currAnimation_EndAd = null;
				break;
			}
		} catch (Exception e) {
			Log.e(Constants.APP_TAG, e.getMessage()
					+ ". Using default animation");
			currAnimation_EndAd = AnimationUtils.loadAnimation(AdmobActivity.MainActivity,
					android.R.anim.slide_out_right);
		}
	}

	public void Hide() {
		
		//layout.setVisibility(View.GONE);
		adView.setVisibility(View.GONE);
		
	}
	
	public void Show() {
		//layout.setVisibility(View.VISIBLE);
		adView.setVisibility(View.VISIBLE);
	}
	
	public void RemoveAdImmediately() {
		
		if(layout != null) {
			adRequest = null;
			adView = null;
			layout.removeAllViews();
			layout = null;
			Debug.Log("BannerAd", DebugLevel.HIGH, "RemoveAdImm: Ad removed");
		}
		else {
			Debug.LogWarning("BannerAd", "RemoveAdImm:: Layout is already NULL");
		}		
	}
	
	public void Reposition(int a_position_1, int a_position_2) {		
		layout.setGravity(a_position_1 | a_position_2);
		Debug.Log("BannerAd", DebugLevel.HIGH, "Reposition:: Repositioned successfully");		
	}

	@Override
	public void onAdLoaded_Internal() {
		// TODO Auto-generated method stub
		super.onAdLoaded_Internal();
		
		if(layout == null) {
			Debug.LogWarning("BannerAdv::OnAdLoaded:: ID'" +getIdentifier() + "'", "Layout is null. This happens when Ad removed OR not initialized good.");
			return;
		}		
		else if(adView == null) {
			Debug.LogWarning("BannerAdv::OnAdLoaded:: ID'" +getIdentifier() + "'", "AdView is null. This happens when Ad removed OR not initialized good.");
			return;
		}
		
		SendToUnity_onAdLoaded();
		
		if(!m_isFirstAdVisible) {
			m_isFirstAdVisible = true;
			
			if (currAnimation_StartAd != null) {
				Debug.Log("BannerAdv::onAdLoaded_Internal:: ",
						DebugLevel.FLOOD,
						"StartAnim set. Triggering Animation: "
								+ getIdentifier());
				layout.startAnimation(currAnimation_StartAd);
				currAnimation_StartAd
						.setAnimationListener(AnimationListenerStartAnim);
			} else {
				Debug.Log("BannerAdv::onAdLoaded_Internal:: ",
						DebugLevel.FLOOD,
						"StartAnim not set. Changing visiblity to true: "
								+ getIdentifier());

				adView.setVisibility(View.VISIBLE);
			}						
		}
		else {
			if (currAnimation_EndAd != null) {
				Debug.Log("BannerAdv::onAdLoaded_Internal:: ", DebugLevel.FLOOD, "EndAnim set. Triggering Animation: "
								+ getIdentifier());
				layout.startAnimation(currAnimation_EndAd);
				currAnimation_EndAd.setAnimationListener(AnimationListenerEndAnim);
			}
		}
	}
		
	@Override
	public void onAdFailedToLoad_Internal(int errorCode) {
		// TODO Auto-generated method stub
		super.onAdFailedToLoad_Internal(errorCode);
		SendToUnity_onAdFailedToLoad(errorCode);
	}

	@Override
	public void onAdOpened_Internal() {
		// TODO Auto-generated method stub
		super.onAdOpened_Internal();
		SendToUnity_onAdOpened();
	}

	@Override
	public void onAdClosed_Internal() {
		// TODO Auto-generated method stub
		super.onAdClosed_Internal();
		SendToUnity_onAdClosed();
	}

	@Override
	public void onAdLeftApplication_Internal() {
		// TODO Auto-generated method stub
		super.onAdLeftApplication_Internal();
		SendToUnity_onAdLeftApplication();
	}



	// region End Animation Listener
	protected Animation.AnimationListener AnimationListenerEndAnim = new Animation.AnimationListener() {

		public void onAnimationStart(Animation animation) {

			Debug.Log(
					"BaseAdvertisement::AnimationListenerEndAnim::onAnimationStart::ID="
							+ getIdentifier(), DebugLevel.HIGH,
					"Animation Start");

		}

		public void onAnimationRepeat(Animation animation) {

		}

		public void onAnimationEnd(Animation animation) {

			if(layout == null) {
				Debug.LogWarning("BannerAdv::EndAnim,onAnimationEnd:: ID'" +getIdentifier() + "'", "Layout is null. This happens when Ad removed OR not initialized good.");
				return;
			}		
			else if(adView == null) {
				Debug.LogWarning("BannerAdv::EndAnim,onAnimationEnd:: ID'" +getIdentifier() + "'", "AdView is null. This happens when Ad removed OR not initialized good.");
				return;
			}
			
			Debug.Log(
					"BaseAdvertisement::AnimationListenerEndAnim::onAnimationEnd::ID="
							+ getIdentifier(), DebugLevel.HIGH, "Animation End");

			if (currAnimation_StartAd != null) {
				layout.startAnimation(currAnimation_StartAd);
			} else {
				adView.setVisibility(View.VISIBLE);
			}
		}
	};

	// region End Animation Listener Only, Dont start new Anim
	protected Animation.AnimationListener AnimationListenerEndAnimOnly = new Animation.AnimationListener() {

		public void onAnimationStart(Animation animation) {

			Debug.Log(
					"BaseAdvertisement::AnimationListenerEndAnimOnly::onAnimationStart::ID="
							+ getIdentifier(), DebugLevel.HIGH,
					"Setup Layout complete");

		}

		public void onAnimationRepeat(Animation animation) {

		}

		public void onAnimationEnd(Animation animation) {

			if(layout == null) {
				Debug.LogWarning("BannerAdv::EndAnimOnly,onAnimationEnd:: ID'" +getIdentifier() + "'", "Layout is null. This happens when Ad removed OR not initialized good.");
				return;
			}		
			else if(adView == null) {
				Debug.LogWarning("BannerAdv::EndAnimOnly,onAnimationEnd:: ID'" +getIdentifier() + "'", "AdView is null. This happens when Ad removed OR not initialized good.");
				return;
			}
			
			Debug.Log(
					"BaseAdvertisement::AnimationListenerEndAnimOnly::onAnimationEnd::ID="
							+ getIdentifier(), DebugLevel.HIGH,
					"Setup Layout complete");

			adView.setVisibility(View.GONE);
		}
	};

	// endregion

	// region Start Animation Listener
	protected Animation.AnimationListener AnimationListenerStartAnim = new Animation.AnimationListener() {

		public void onAnimationStart(Animation animation) {

			Debug.Log(
					"BaseAdvertisement::AnimationListenerStartAnim::onAnimationStart::ID="
							+ getIdentifier(), DebugLevel.HIGH,
					"Animation Start");

		}

		public void onAnimationRepeat(Animation animation) {

		}

		public void onAnimationEnd(Animation animation) {

			if(layout == null) {
				Debug.LogWarning("BannerAdv::StartAnim,onAnimationEnd:: ID'" +getIdentifier() + "'", "Layout is null. This happens when Ad removed OR not initialized good.");
				return;
			}		
			else if(adView == null) {
				Debug.LogWarning("BannerAdv::StartAnim,onAnimationEnd:: ID'" +getIdentifier() + "'", "AdView is null. This happens when Ad removed OR not initialized good.");
				return;
			}
			
			Debug.Log(
					"BaseAdvertisement::AnimationListenerStartAnim::onAnimationEnd::ID="
							+ getIdentifier(), DebugLevel.HIGH, "Animation End");

			adView.setVisibility(View.VISIBLE);
		}
	};
	
}
