package com.microeyes.admob; //jp.radiumsoftware.unityplugin.admob;

import java.util.Hashtable;

import com.microeyes.utils.Constants;
import com.microeyes.utils.Debug;
import com.microeyes.utils.Debug.DebugLevel;
import com.unity3d.player.UnityPlayer;

import android.app.Activity;
import android.util.Log;

import com.microeyes.admob.catalog.EAdType;
import com.microeyes.admob.catalog.EBannerAdType;


public class AdmobActivity { //implements AdListener{
	//public static AdView adView;
	static Activity MainActivity;
	
	public static String AD_UNIT_ID_COMMON = ""; // "a14e2fb60918999";
	//public static String ADV_PUB_ID_INTERSTITIAL = ""; // "a14e2fb60918999";
    public static boolean isTestMode = true;
    static boolean hasGotAdv = false;
	public static String testDeviceId = "6F1CC2C5371933EEDDACB0BF7F42917F";
	static String callbackGOName="";
	
	static Hashtable<String, BaseAdvertisement> listAdv = new Hashtable<String, BaseAdvertisement>();
	
	/*static int s_requestId = 0;
	static int getRequestIdNew() {
		return ++s_requestId;
	}*/
	
    static public void Initialize(final Activity CurrActivity, final String PUB_ID_BANNER,  
    		//final String PUB_ID_INTERSTITIAL,
    		final boolean isTesting, 
    		final String TestDeviceID, 
    		final String CallbackGameObjName, final int LevelOfDebug)
    {
    	//Debug.Log("AdmobActivity::Initialize", DebugLevel.LOW, "Requested: Initialize AdView Start");
    	Log.i(Constants.APP_TAG, "Requested: Initialize AdView Start");    
    	
    	Log.i(Constants.APP_TAG, "Initialize::Initializing: Received PUB_ID_BANNER: " + PUB_ID_BANNER + //", PUB_ID_INTERSTITIAL: " + PUB_ID_INTERSTITIAL + 
    			"  isTesting: " + isTesting  + 
    			"  callback obj: " + CallbackGameObjName + 
    			"   levelOfDebug: " + LevelOfDebug);
    	AdmobActivity.MainActivity = CurrActivity;
    	
    	Debug.SetDebugLevel(LevelOfDebug);
    	
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				Debug.Log("Initialize", DebugLevel.LOW, "Initialize Start");
		    	AdmobActivity.AD_UNIT_ID_COMMON = PUB_ID_BANNER;
				AdmobActivity.isTestMode = isTesting;
				AdmobActivity.testDeviceId = TestDeviceID;				
				AdmobActivity.callbackGOName = CallbackGameObjName;
				Debug.Log("Initialize", DebugLevel.LOW, "Initialize End");
			}
    	});
		
        Log.i(Constants.APP_TAG, "Requested: Initialize AdView End");
    }
    
    static public void Banner_Create(final String a_identifier, final String a_adUnitId, final int a_bannerType, final int a_orientation, final int a_pos_1, final int a_pos_2,
    		final int a_animationInType, final int a_animationOutType)
    {
    	Log.i(Constants.APP_TAG, "CreateBanner:: identifier: " + a_identifier + " START");

    	Debug.Log("CreateBanner", DebugLevel.LOW, "  ADV_SIZE: " + a_bannerType + 
    			"   Orientation: " + a_orientation + "  pos_1: " + a_pos_1 + "   pos_2: " + a_pos_2 + 
    			"   AnimationInType: " + a_animationInType + "   AnimationOutType: " + a_animationOutType);
    	
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {			
				Debug.Log("CreateNewAdv", DebugLevel.FLOOD, " Adv Type BANNER: " + EBannerAdType.values()[a_bannerType]);
	    		BannerAdv bannerAdv = new BannerAdv(a_identifier, EBannerAdType.values()[a_bannerType], a_orientation, a_animationInType, a_animationOutType);
	    		bannerAdv.SetPositions(a_pos_1, a_pos_2);
	    		if(a_adUnitId.trim().isEmpty()) {
	    			Debug.Log("CreateBanner", DebugLevel.FLOOD, "Using Common AD_UNIT_ID");
	    			bannerAdv.SetAdUnitID(AD_UNIT_ID_COMMON);
	    		}
	    		else {
	    			Debug.Log("CreateBanner", DebugLevel.FLOOD, "Using new AD_UNIT_ID");
	    			bannerAdv.SetAdUnitID(a_adUnitId);
	    		}
	    		listAdv.put(a_identifier, bannerAdv);
			}
    	});
    	
    	Log.i(Constants.APP_TAG, "CreateBanner:: identifier: " + a_identifier + " END");
    }
    
    static public void Banner_Load(final String a_identifier) {
    	
    	Log.i(Constants.APP_TAG, "LoadBanner:: identifier: " + a_identifier + " START");
    	
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {			
				Debug.Log("LoadBanner", DebugLevel.LOW, " Adv Type BANNER");
	    		
				BannerAdv l_bannerAdv = (BannerAdv) listAdv.get(a_identifier);
				if(l_bannerAdv != null) {
					l_bannerAdv.Load();
				}
				else {
					Debug.LogWarning("LoadBanner", "No BannerAd found with ID: '" + a_identifier + "'");
				}					
				
			}
    	});
    	
    	Log.i(Constants.APP_TAG, "LoadBanner:: identifier: " + a_identifier + " END");
    }
    
    static public void Banner_Hide(final String a_identifier) {
    	Log.i(Constants.APP_TAG, "ShowBanner:: identifier: " + a_identifier + " START");
    	
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {			
				Debug.Log("ShowBanner", DebugLevel.LOW, " Adv Type BANNER");
	    		
				BannerAdv l_bannerAdv = (BannerAdv) listAdv.get(a_identifier);
				if(l_bannerAdv != null) {
					l_bannerAdv.Show();
				}
				else {
					Debug.LogWarning("ShowBanner", "No BannerAd found with ID: '" + a_identifier + "'");
				}					
					
				}
    	});
    	
    	Log.i(Constants.APP_TAG, "ShowBanner:: identifier: " + a_identifier + " END");
    }
    
    static public void Banner_Show(final String a_identifier) {
    	Log.i(Constants.APP_TAG, "HideBanner:: identifier: " + a_identifier + " START");
    	
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {			
				Debug.Log("HideBanner", DebugLevel.LOW, " Adv Type BANNER");
	    		
				BannerAdv l_bannerAdv = (BannerAdv) listAdv.get(a_identifier);
				if(l_bannerAdv != null) {
					l_bannerAdv.Hide();
				}
				else {
					Debug.LogWarning("Banner_Show", "No BannerAd found with ID: '" + a_identifier + "'");
				}					
				
			}
    	});
    	
    	Log.i(Constants.APP_TAG, "HideBanner:: identifier: " + a_identifier + " END");
    }
    
    static public void Interstitial_Create(final String a_identifier, final String a_adUnitId) {
    	Log.i(Constants.APP_TAG, "Interstitial_Create:: identifier: " + a_identifier + " START");
    	
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {			
				Debug.Log("Interstitial_Create", DebugLevel.LOW, " Adv Type BANNER");
	    		
				InterstitialAdv l_interAdv = new InterstitialAdv(a_identifier);
				if(a_adUnitId.trim().isEmpty()) {
	    			Debug.Log("Interstitial_Create", DebugLevel.FLOOD, "Using Common AD_UNIT_ID");
	    			l_interAdv.SetAdUnitID(AD_UNIT_ID_COMMON);
	    		}
	    		else {
	    			Debug.Log("Interstitial_Create", DebugLevel.FLOOD, "Using new AD_UNIT_ID");
	    			l_interAdv.SetAdUnitID(a_adUnitId);
	    		}
	    		listAdv.put(a_identifier, l_interAdv);
				
			}
    	});
    	
    	Log.i(Constants.APP_TAG, "Interstitial_Create:: identifier: " + a_identifier + " END");
    }
    
    static public void Interstitial_Load(final String a_identifier) {
    	Log.i(Constants.APP_TAG, "Interstitial_Load:: identifier: " + a_identifier + " START");
    	
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {			
				Debug.Log("Interstitial_Load", DebugLevel.LOW, " Adv Type BANNER");
	    		
				InterstitialAdv l_interAdv = (InterstitialAdv) listAdv.get(a_identifier);
				if(l_interAdv != null) {
					l_interAdv.Load();
				}
				else {
					Debug.LogWarning("Interstitial_Load", "No InterstitialAdv found with ID: '" + a_identifier + "'");
				}				
			}
    	});
    	
    	Log.i(Constants.APP_TAG, "Interstitial_Load:: identifier: " + a_identifier + " END");
    }
    
    static public void Interstitial_Show(final String a_identifier) {
    	Log.i(Constants.APP_TAG, "Interstitial_Show:: identifier: " + a_identifier + " START");
    	
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {			
				Debug.Log("Interstitial_Show", DebugLevel.LOW, " Adv Type BANNER");
	    		
				InterstitialAdv l_interAdv = (InterstitialAdv) listAdv.get(a_identifier);
				if(l_interAdv != null) {
					l_interAdv.Show();
				}
				else {
					Debug.LogWarning("Interstitial_Show", "No InterstitialAdv found with ID: '" + a_identifier + "'");
				}
				
			}
    	});
    	
    	Log.i(Constants.APP_TAG, "Interstitial_Show:: identifier: " + a_identifier + " END");
    }
    
    static public void Interstitial_IsLoaded(final String a_identifier) {
    	Log.i(Constants.APP_TAG, "Interstitial_IsLoaded:: identifier: " + a_identifier + " START");    	
	        	
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {			
				Debug.Log("Interstitial_Load", DebugLevel.LOW, " Adv Type BANNER");
				
				boolean l_isLoaded = false;
				int l_iLoaded = 0;
		    	InterstitialAdv l_interAdv = (InterstitialAdv) listAdv.get(a_identifier);
		    	if(l_interAdv != null) {
		    		l_isLoaded = l_interAdv.IsLoaded();
		    	}
		    	else {
		    		Debug.LogWarning("Interstitial_IsLoaded", "No InterstitialAdv found with ID: '" + a_identifier + "'");
		    	}
				
		    	l_iLoaded = l_isLoaded == true? 1 : 0;
		    	
		    	UnityPlayer.UnitySendMessage(callbackGOName, "OnRequestCompleted", a_identifier + "^" + InterstitialAdv.Func_IsLoaded + "^" + l_iLoaded);
		    	
				Debug.Log("Interstitial_Load", DebugLevel.LOW, " Is Loaded: " + l_interAdv.IsLoaded());
			}
    	});
				
    	Log.i(Constants.APP_TAG, "Interstitial_IsLoaded:: identifier: " + a_identifier + " END");    	
    }
    
    static public void RemoveAd(final String a_identifier) {
    	
    	Log.i(Constants.APP_TAG, "RemoveAd:: identifier: " + a_identifier + " START");
    	
		
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {	
				BaseAdvertisement l_baseAdv = (BaseAdvertisement) listAdv.get(a_identifier);
		    	if(l_baseAdv != null) {
		    			
		    		listAdv.remove(a_identifier);
		    		Debug.Log("RemoveAd", DebugLevel.HIGH, "Removed Ad '" + a_identifier + "'");
		    	}
		    	else {
		    		Debug.LogWarning("RemoveAd", "Can't find ID '" + a_identifier + "' to remove");
		    	}
			}
		});
    	
    	Log.i(Constants.APP_TAG, "RemoveAd:: identifier: " + a_identifier + " END");    	
    }
    
    static public void RemoveAdImmediately(final String a_identifier) {
    	
    	Log.i(Constants.APP_TAG, "RemoveAdImmediately:: identifier: " + a_identifier + " START");
    	
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {	
		    	BaseAdvertisement l_baseAdv = (BaseAdvertisement) listAdv.get(a_identifier);
		    	if(l_baseAdv != null) {
		    		
		    		if(l_baseAdv.getAdvType() == EAdType.BANNER) {
		    			BannerAdv l_bannerAd = (BannerAdv) l_baseAdv;		    			
		    			l_bannerAd.RemoveAdImmediately();
		    		}
		    		else if(l_baseAdv.getAdvType() == EAdType.INTERSTITIAL) {
		    			
		    		}	
		    		
		    		listAdv.remove(a_identifier);
		    		Debug.Log("RemoveAdImmediately", DebugLevel.HIGH, "Removed Ad '" + a_identifier + "'");
		    	}
		    	else {
		    		Debug.LogWarning("RemoveAdImmediately", "Can't find ID '" + a_identifier + "' to remove");
		    	}
			}
    	});
    	
    	Log.i(Constants.APP_TAG, "RemoveAdImmediately:: identifier: " + a_identifier + " END");    	
    }
    
    static public void Banner_Reposition(final String a_identifier, final int a_position_1, final int a_position_2) {
    	
    	Log.i(Constants.APP_TAG, "Banner_Reposition:: identifier: " + a_identifier + " START");
    	
    	AdmobActivity.MainActivity.runOnUiThread(new Runnable() {			
			@Override
			public void run() {	
			
				BannerAdv l_bannerAd = (BannerAdv) listAdv.get(a_identifier);
				if(l_bannerAd != null) {
					l_bannerAd.Reposition(a_position_1, a_position_2);
				}
				else {
					Debug.LogWarning("BannerReposition", "Can't find ID: '" + a_identifier + "'");
				}				
			}
    	});
    	
    	Log.i(Constants.APP_TAG, "Banner_Reposition:: identifier: " + a_identifier + " END");
    }
        
    //region Admob events Callbacks to Unity
	static public void onAdOpened(String a_identifier) {
		
		Log.i(Constants.APP_TAG, "AdmobActivity::onAdOpened:: " + a_identifier);

        if(!callbackGOName.trim().isEmpty())
        	UnityPlayer.UnitySendMessage(callbackGOName, "onAdOpened", a_identifier);
	}

	static public void onAdFailedToLoad(String a_identifier, int arg1) {
				
		Log.i(Constants.APP_TAG, "AdmobActivity::onAdFailedToLoad:: " + a_identifier + " ___ErrorCode: " + arg1);

        if(!callbackGOName.trim().isEmpty())
        	UnityPlayer.UnitySendMessage(callbackGOName, "onAdFailedToLoad", a_identifier + "^" + arg1);
	}

	static public void onAdLeftApplication(String a_identifier) {

		Log.i(Constants.APP_TAG, "AdmobActivity::onAdLeftApplication:: " + a_identifier);

        if(!callbackGOName.trim().isEmpty())
        	UnityPlayer.UnitySendMessage(callbackGOName, "onAdLeftApplication", a_identifier);
        
	}

	static public void onAdClosed(String a_identifier) {
		Log.i(Constants.APP_TAG, "AdmobActivity::onAdClosed:: " + a_identifier);

        if(!callbackGOName.trim().isEmpty())
        	UnityPlayer.UnitySendMessage(callbackGOName, "onAdClosed", a_identifier);
	}

	static public void onAdLoaded(String a_identifier) {
		
		Log.i(Constants.APP_TAG, "AdmobActivity::onAdLoaded:: " + a_identifier);

        if(!callbackGOName.trim().isEmpty())
        	UnityPlayer.UnitySendMessage(callbackGOName, "onAdLoaded", a_identifier);
	}
    //endregion qwe
 
}
