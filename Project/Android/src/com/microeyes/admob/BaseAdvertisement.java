package com.microeyes.admob;

import android.util.Log;

import com.google.android.gms.ads.*;
import com.microeyes.admob.catalog.EAdType;
import com.microeyes.utils.Constants;

public class BaseAdvertisement {

	private String m_identifier = "";
	protected AdRequest adRequest = null;
	protected EAdType advType = EAdType.BANNER;

	protected int advOrientation = -1;
	
	protected String m_adUnitId = "";

	protected BaseAdvertisement(String a_identifier) {
		m_identifier = a_identifier;
	}

	public EAdType getAdvType() {
		return advType;
	}

	protected void setAdvType(EAdType advType) {
		this.advType = advType;
	}

	public String getIdentifier() {
		return m_identifier;
	}
	
	public void SetAdUnitID(String a_adUnitId) {
		m_adUnitId = a_adUnitId;
	}
	
	protected String getAdUnitId() {		
		return m_adUnitId;
	}

	// region Admob events Callbacks to Unity
	public void onAdLoaded_Internal() {
		Log.i(Constants.APP_TAG, "BaseAdvertisement::onAdLoaded_Internal:: " +
				  getIdentifier());
		 //AdmobActivity.onAdLoaded(getIdentifier());
	}
	
	protected void SendToUnity_onAdLoaded() {
		AdmobActivity.onAdLoaded(getIdentifier());
	}

	public void onAdFailedToLoad_Internal(int errorCode) {
		Log.i(Constants.APP_TAG,
				 "BaseAdvertisement::onAdFailedToLoad_Internal:: " + getIdentifier() );
		
		//AdmobActivity.onAdFailedToLoad(getIdentifier(), errorCode);
	}
	
	protected void SendToUnity_onAdFailedToLoad(int errorCode) {
		AdmobActivity.onAdFailedToLoad(getIdentifier(), errorCode);
	}

	public void onAdOpened_Internal() {
		Log.i(Constants.APP_TAG,
				 "BaseAdvertisement::onAdOpened_Internal:: " + getIdentifier());
		 //AdmobActivity.onAdOpened(getIdentifier());
	}
	
	protected void SendToUnity_onAdOpened() {
		AdmobActivity.onAdOpened(getIdentifier());
	}
	
	public void onAdClosed_Internal() {
		Log.i(Constants.APP_TAG,
				"BaseAdvertisement::onAdClosed_Internal:: " + getIdentifier());
		 //AdmobActivity.onAdClosed(getIdentifier());
	}
	
	protected void SendToUnity_onAdClosed() {
		AdmobActivity.onAdClosed(getIdentifier());
	}

	public void onAdLeftApplication_Internal() {
		Log.i(Constants.APP_TAG,
				  "BaseAdvertisement::onAdLeftApplication_Internal:: " + getIdentifier());
	 	//AdmobActivity.onAdLeftApplication(getIdentifier());
	}

	protected void SendToUnity_onAdLeftApplication() {
		AdmobActivity.onAdLeftApplication(getIdentifier());
	}
	
	// endregion

}
